package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class MarieIgoe_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	ArrayList<Song> daveMatthewsBandTracks = new ArrayList<Song>();
    DaveMatthewsBand theDaveMatthewsBand = new DaveMatthewsBand();
	
    daveMatthewsBandTracks = theDaveMatthewsBand.getDaveMatthewsBandSongs();
	
	playlist.add(daveMatthewsBandTracks.get(0));
	playlist.add(daveMatthewsBandTracks.get(1));
	playlist.add(daveMatthewsBandTracks.get(2));
	
	HootieAndTheBlowfish hootieAndTheBlowfishBand = new HootieAndTheBlowfish();
	ArrayList<Song> hootieAndTheBlowfishTracks = new ArrayList<Song>();
	hootieAndTheBlowfishTracks = hootieAndTheBlowfishBand.getHootieAndTheBlowfishSongs();
	
	
	playlist.add(hootieAndTheBlowfishTracks.get(0));
	playlist.add(hootieAndTheBlowfishTracks.get(1));
	playlist.add(hootieAndTheBlowfishTracks.get(2));
	
	

    return playlist;
	}
}
