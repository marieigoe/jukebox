package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class FortMinor {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public FortMinor() {
    }
    
    public ArrayList<Song> getFortMinorSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Petrified", "Fort Minor");                //Create a song
         Song track2 = new Song("In Stereo", "Fort Minor");               //Create another song
         Song track3 = new Song("Remember the Name", "Fort Minor");                         //Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Issues
         this.albumTracks.add(track2);                                          //Add the second song to song list for Issues
         this.albumTracks.add(track3);                                          //Add the third song to song list for Issues
         return albumTracks;                                                    //Return the songs for Fort Minor in the form of an ArrayList
    }
}
