package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class HootieAndTheBlowfish {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public HootieAndTheBlowfish() {
    }
    
    public ArrayList<Song> getHootieAndTheBlowfishSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   		//Instantiate the album so we can populate it below
    	 Song track1 = new Song("Only Wanna Be With You", "Hootie And The Blowfish");   //Create a song
         Song track2 = new Song("Hold On", "Hootie And The Blowfish");         			//Create another song
         Song track3 = new Song("One Love", "Hootie And The Blowfish");         		//Create another song
         this.albumTracks.add(track1);                                          		//Add the first song to song list for Hootie And The Blowfish
         this.albumTracks.add(track2); 													//Add the second song to song list for Hootie And The Blowfish 
         this.albumTracks.add(track3); 													//Add the third song to song list for Hootie And The Blowfish
         return albumTracks;                                                    		//Return the songs for Hootie And The Blowfish in the form of an ArrayList
    }
}
