package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class DaveMatthewsBand {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public DaveMatthewsBand() {
    }
    
    
    public ArrayList<Song> getDaveMatthewsBandSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   	//Instantiate the album so we can populate it below
    	 Song track1 = new Song("Crash Into Me", "Dave Matthews Band");         	//Create a song
         Song track2 = new Song("Satelite", "Dave Matthews Band");  				//Create another song
         Song track3 = new Song("Crush", "Dave Matthews Band");         			//Create another song
         this.albumTracks.add(track1);                                          	//Add the first song to song list for Dave Matthews Band
         this.albumTracks.add(track2); 												//Add the second song to song list for Dave Matthews Band
         this.albumTracks.add(track3); 												//Add the third song to song list for Dave Matthews Band
         return albumTracks;                                                    	//Return the songs for Dave Matthews Band in the form of an ArrayList
    }
}
